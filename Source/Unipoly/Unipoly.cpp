// Copyright Epic Games, Inc. All Rights Reserved.

#include "Unipoly.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Unipoly, "Unipoly" );
