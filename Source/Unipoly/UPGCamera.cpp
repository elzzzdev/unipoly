// Fill out your copyright notice in the Description page of Project Settings.

#include "UPGCamera.h"

#include "Kismet/GameplayStatics.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/KismetMathLibrary.h"

AUPGCamera::AUPGCamera()
{
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComponent"));
	SpringArmComponent->TargetArmLength = 1440.f;
	SpringArmComponent->bDoCollisionTest = false;
	SpringArmComponent->SetRelativeRotation(FRotator(-40.f, 0.f, 0.f));
	SpringArmComponent->SetupAttachment(RootComponent);

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	CameraComponent->FieldOfView = 35.f;
	CameraComponent->PostProcessSettings.bOverride_DepthOfFieldFstop = true;
	CameraComponent->PostProcessSettings.bOverride_DepthOfFieldSensorWidth = true;
	CameraComponent->PostProcessSettings.bOverride_DepthOfFieldFocalDistance = true;
	CameraComponent->PostProcessSettings.DepthOfFieldFstop = 1.f;
	CameraComponent->PostProcessSettings.DepthOfFieldSensorWidth = 350.f;
	CameraComponent->PostProcessSettings.DepthOfFieldFocalDistance = SpringArmComponent->TargetArmLength - 20.f;
	CameraComponent->SetRelativeRotation(FRotator(4.f, 0.f, 0.f));
	CameraComponent->SetupAttachment(SpringArmComponent);
}

void AUPGCamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	MovementTimeline.TickTimeline(DeltaTime);
}

void AUPGCamera::SetAlwaysRotateToAnchor(bool NewAlwaysRotateToAnchor)
{
	bAlwaysRotateToAnchor = NewAlwaysRotateToAnchor;
}

bool AUPGCamera::IsAlwaysRotateToAnchor() const
{
	return bAlwaysRotateToAnchor;
}

void AUPGCamera::NetMulticast_MoveToLocation_Implementation(const FVector& Destination)
{
	EndLocation = Destination;
	StartLocation = GetActorLocation();
	MovementTimeline.PlayFromStart();
}

void AUPGCamera::MovementTimelineProgress(float Alpha)
{
	SetActorLocation(FMath::Lerp(StartLocation, EndLocation, Alpha));
	if (bAlwaysRotateToAnchor)
	{
		const FRotator AnchorRotation = UKismetMathLibrary::GetDirectionUnitVector(GetActorLocation(), RotationAnchor->GetActorLocation()).Rotation();
		SetActorRotation(FRotator(0, AnchorRotation.Yaw, 0));
	}
}

void AUPGCamera::MovementTimelineFinished()
{

}

void AUPGCamera::BeginPlay()
{
	Super::BeginPlay();

	if (MovementCurveFloat)
	{
		FOnTimelineFloat onMovementTimelineProgress;
		FOnTimelineEventStatic onMovementTimelineFinished;

		onMovementTimelineProgress.BindUFunction(this, FName("MovementTimelineProgress"));
		MovementTimeline.AddInterpFloat(MovementCurveFloat, onMovementTimelineProgress);

		onMovementTimelineFinished.BindUFunction(this, FName("MovementTimelineFinished"));
		MovementTimeline.SetTimelineFinishedFunc(onMovementTimelineFinished);
	}
}

