#include "UPGUWPlayerPlate.h"

#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "GameFramework/HUD.h"

void UUPGUWPlayerPlate::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);
	UE_LOG(LogTemp, Warning, TEXT("Player Name Size: %f %f"), NicknameLabel->GetDesiredSize().X, NicknameLabel->GetDesiredSize().Y);
}

void UUPGUWPlayerPlate::SetProfileImage(UTexture2D* NewImage) const
{
	ProfileImage->SetBrushFromTexture(NewImage);
}

void UUPGUWPlayerPlate::SetStrokeColour(const FColor& NewColour) const
{
	StrokeImage->SetBrushTintColor(FSlateColor(NewColour));
}

void UUPGUWPlayerPlate::SetNickname(FString NewNickname) const
{
	// Collapse name if it's too long, judge by length because we use a monospaced font
	if (NewNickname.Len() > 16)
	{
		NewNickname = NewNickname.Left(13) + TEXT("...");
	}
	NicknameLabel->SetText(FText::FromString(NewNickname));
}

void UUPGUWPlayerPlate::SetManaAmount(const uint16 NewManaAmount)
{
	ManaAmount = NewManaAmount;
	ManaLabel->SetText(FText::FromString(FString::Printf(TEXT("Mana: %d"), NewManaAmount)));
}

void UUPGUWPlayerPlate::AddManaAmount(const uint16 Amount)
{
	SetManaAmount(ManaAmount += Amount);
}

void UUPGUWPlayerPlate::SubtractManaAmount(const uint16 Amount)
{
	SetManaAmount(ManaAmount -= Amount);
}

void UUPGUWPlayerPlate::SetKnowledgeAmount(const uint16 NewKnowledgeAmount)
{
	KnowledgeAmount = NewKnowledgeAmount;
	KnowledgeLabel->SetText(FText::FromString(FString::Printf(TEXT("Knowledge: %d"), NewKnowledgeAmount)));
}

void UUPGUWPlayerPlate::PlayShowAnimation()
{
	PlayAnimation(Show);
}

void UUPGUWPlayerPlate::PlayHideAnimation()
{
	PlayAnimation(Show, 0, 1, EUMGSequencePlayMode::Reverse,  1, false);
}

void UUPGUWPlayerPlate::PlayPickAnimation()
{
	PlayAnimation(Pick);
}

void UUPGUWPlayerPlate::PlayDiscardAnimation()
{
	PlayAnimation(Pick, 0, 1, EUMGSequencePlayMode::Reverse, 1, false);
}

void UUPGUWPlayerPlate::AddKnowledgeAmount(const uint16 Amount)
{
	SetKnowledgeAmount(KnowledgeAmount += Amount);
}

void UUPGUWPlayerPlate::SubtractKnowledgeAmount(const uint16 Amount)
{
	SetKnowledgeAmount(KnowledgeAmount -= Amount);
}

void UUPGUWPlayerPlate::NativeConstruct()
{
	Super::NativeConstruct();

}
