#include "UPGUWPendingHint.h"

#include "Components/HorizontalBox.h"
#include "Components/TextBlock.h"
#include "Components/VerticalBox.h"

void UUPGUWPendingHint::ShowWaitForPlayer(const FText& Nickname, const FColor Colour)
{
	NicknameLabel->SetText(Nickname);
	NicknameLabel->SetColorAndOpacity(Colour);
	SetVisibility(ESlateVisibility::Visible);
	PressBox->SetVisibility(ESlateVisibility::Collapsed);
	WaitBox->SetVisibility(ESlateVisibility::Visible);
}

void UUPGUWPendingHint::ShowPressHint()
{
	SetVisibility(ESlateVisibility::Visible);
	PressBox->SetVisibility(ESlateVisibility::Visible);
	WaitBox->SetVisibility(ESlateVisibility::Collapsed);
}

void UUPGUWPendingHint::Hide()
{
	SetVisibility(ESlateVisibility::Hidden);
}

void UUPGUWPendingHint::NativeConstruct()
{
	Super::NativeConstruct();
	PressBox->SetVisibility(ESlateVisibility::Collapsed);
	WaitBox->SetVisibility(ESlateVisibility::Collapsed);
}
