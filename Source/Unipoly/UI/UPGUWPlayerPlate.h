// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UPGUserWidget.h"
#include "UPGUWPlayerPlate.generated.h"

class UTextBlock;
class UImage;

UCLASS()
class UNIPOLY_API UUPGUWPlayerPlate : public UUPGUserWidget
{
	GENERATED_BODY()

public:
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	void SetProfileImage(UTexture2D* NewImage) const;

	void SetStrokeColour(const FColor& NewColour) const;

	void SetNickname(FString NewNickname) const;

	void SetManaAmount(const uint16 NewManaAmount);

	void AddManaAmount(const uint16 Amount);

	void SubtractManaAmount(const uint16 Amount);

	void AddKnowledgeAmount(const uint16 Amount);

	void SubtractKnowledgeAmount(const uint16 Amount);

	void SetKnowledgeAmount(const uint16 NewKnowledgeAmount);

	void PlayShowAnimation();

	void PlayHideAnimation();

	void PlayPickAnimation();

	void PlayDiscardAnimation();

protected:
	virtual void NativeConstruct() override;

	UPROPERTY(meta = (BindWidget))
	UImage* StrokeImage;

	UPROPERTY(meta = (BindWidget))
	UImage* ProfileImage;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* NicknameLabel;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* ManaLabel;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* KnowledgeLabel;

	UPROPERTY(Transient, meta = (BindWidgetAnim))
	UWidgetAnimation* Show;

	UPROPERTY(Transient, meta = (BindWidgetAnim))
	UWidgetAnimation* Pick;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TArray<FColor> ColourPalette;

	uint16 ManaAmount = 0;

	uint16 KnowledgeAmount = 0;
};
