// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UPGUserWidget.h"
#include "UPGUWPendingHint.generated.h"

class UHorizontalBox;
class UVerticalBox;
class UTextBlock;

UCLASS()
class UNIPOLY_API UUPGUWPendingHint : public UUPGUserWidget
{
	GENERATED_BODY()

public:
	void ShowWaitForPlayer(const FText& Nickname, const FColor Colour);

	void ShowPressHint();

	void Hide();

private:
	virtual void NativeConstruct() override;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* NicknameLabel;

	UPROPERTY(meta = (BindWidget))
	UVerticalBox* WaitBox;

	UPROPERTY(meta = (BindWidget))
	UHorizontalBox* PressBox;
};
