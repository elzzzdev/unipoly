#include "UPGUWPlayersDeck.h"

#include "UPGUWPlayerPlate.h"
#include "Blueprint/WidgetTree.h"
#include "Components/CanvasPanelSlot.h"
#include "Components/VerticalBox.h"
#include "Components/VerticalBoxSlot.h"
#include "Unipoly/GameplayFramework/UPGPlayerState.h"

uint8 UUPGUWPlayersDeck::Size() const
{
	return Plates.Num();
}

UUPGUWPlayerPlate* UUPGUWPlayersDeck::AddPlayerPlate(const AUPGPlayerState* Player)
{
	UUPGUWPlayerPlate* NewPlate = WidgetTree->ConstructWidget<UUPGUWPlayerPlate>(PlayerPlateClass, FName(*FString::FromInt(Player->GetUniqueID())));
	Plates.Add(Player->GetUniqueID(), NewPlate);
	VerticalBox->AddChild(NewPlate);
	UVerticalBoxSlot* PlateSlot = Cast<UVerticalBoxSlot>(NewPlate->Slot);
	PlateSlot->SetPadding(FMargin(0, PlatesGap, 0, 0));

	NewPlate->SetStrokeColour(Player->GetPlayerColour());
	NewPlate->SetNickname(Player->GetPlayerName());
	NewPlate->SetProfileImage(Player->GetPlayerAvatar());

	RefreshSizeAdjustment();
	// NewPlate->PlayShowAnimation();
	return NewPlate;
}

UUPGUWPlayerPlate* UUPGUWPlayersDeck::GetPlayerPlate(const AUPGPlayerState* Player)
{
	return GetPlayersPlateById(Player->GetUniqueID());
}

UUPGUWPlayerPlate* UUPGUWPlayersDeck::GetPlayersPlateById(const uint32 PlayerId)
{
	return Plates[PlayerId];
}

void UUPGUWPlayersDeck::PickPlayer(const AUPGPlayerState* Player)
{
	CurrentPick = GetPlayerPlate(Player);
	CurrentPick->PlayPickAnimation();
}

void UUPGUWPlayersDeck::DiscardCurrentPick()
{
	if (CurrentPick)
	{
		CurrentPick->PlayDiscardAnimation();
		CurrentPick = nullptr;
	}
}

void UUPGUWPlayersDeck::NativeConstruct()
{
	Super::NativeConstruct();

}

void UUPGUWPlayersDeck::RefreshSizeAdjustment() const
{
	UCanvasPanelSlot* BoxSlot = Cast<UCanvasPanelSlot>(VerticalBox->Slot);
	const float NewHeight = (PlateHeight + PlatesGap) * Size();
	BoxSlot->SetSize(FVector2D(BoxSlot->GetSize().X, NewHeight));
}
