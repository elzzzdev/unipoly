// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DiceCapture2D.generated.h"

UCLASS()
class UNIPOLY_API ADiceCapture2D : public AActor
{
	GENERATED_BODY()

public:
	ADiceCapture2D();

	virtual void Tick(float DeltaTime) override;
	
	void Roll(const uint8 Result);
	
	void Hide() const;

protected:
	virtual void BeginPlay() override;
	
	UPROPERTY(EditAnywhere)
	USkeletalMeshComponent* DiceSkeletalMeshComponent;

	UPROPERTY(EditAnywhere)
	USceneCaptureComponent2D* SceneCaptureComponent2D;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	USkeletalMesh* DiceOneMesh;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	USkeletalMesh* DiceTwoMesh;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	USkeletalMesh* DiceThreeMesh;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	USkeletalMesh* DiceFourMesh;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	USkeletalMesh* DiceFiveMesh;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	USkeletalMesh* DiceSixMesh;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TArray<UAnimSequence*> RollAnimations;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	UAnimSequence* DiceHideAnimation;
};
