// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UPGUserWidget.h"
#include "UPGUWPauseMenu.generated.h"

class UButton;

UCLASS()
class UNIPOLY_API UUPGUWPauseMenu : public UUPGUserWidget
{
	GENERATED_BODY()

protected:
	virtual void NativeConstruct() override;

	UFUNCTION()
    void OnContinueButtonClicked();

	UFUNCTION()
	void OnSettingsButtonClicked();

	UFUNCTION()
	void OnQuitGameButtonClicked();

	UPROPERTY(meta = (BindWidget))
    UButton* ContinueButton;

	UPROPERTY(meta = (BindWidget))
    UButton* SettingsButton;

	UPROPERTY(meta = (BindWidget))
    UButton* QuitGameButton;
};
