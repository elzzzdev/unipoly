// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UPGUserWidget.h"
#include "UPGUWPlayersDeck.generated.h"

class AUPGPlayerState;
class UUPGUWPlayerPlate;
class UVerticalBox;

UCLASS()
class UNIPOLY_API UUPGUWPlayersDeck : public UUPGUserWidget
{
	GENERATED_BODY()

public:
	uint8 Size() const;

	UUPGUWPlayerPlate* AddPlayerPlate(const AUPGPlayerState* Player);

	UUPGUWPlayerPlate* GetPlayerPlate(const AUPGPlayerState* Player);

	UUPGUWPlayerPlate* GetPlayersPlateById(const uint32 PlayerId);

	void PickPlayer(const AUPGPlayerState* Player);

	void DiscardCurrentPick();

protected:
	virtual void NativeConstruct() override;

	void RefreshSizeAdjustment() const;

	UPROPERTY(meta = (BindWidget))
	UVerticalBox* VerticalBox;

	UPROPERTY(EditDefaultsOnly)
	float PlatesGap = 10.f;

	const float PlateHeight = 68.f;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UUPGUWPlayerPlate> PlayerPlateClass;

	TMap<int32, UUPGUWPlayerPlate*> Plates;

	UPROPERTY()
	UUPGUWPlayerPlate* CurrentPick;
};
