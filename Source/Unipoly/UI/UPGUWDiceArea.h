// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UPGUserWidget.h"
#include "UPGUWDiceArea.generated.h"

UCLASS()
class UNIPOLY_API UUPGUWDiceArea : public UUPGUserWidget
{
	GENERATED_BODY()

	protected:
	virtual void NativeConstruct() override;
};
