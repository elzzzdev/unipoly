#include "UPGUWPauseMenu.h"

#include "Kismet/KismetSystemLibrary.h"
#include "Components/Button.h"

void UUPGUWPauseMenu::NativeConstruct()
{
    Super::NativeConstruct();

    ContinueButton->OnClicked.AddUniqueDynamic(this, &UUPGUWPauseMenu::OnContinueButtonClicked);
    SettingsButton->OnClicked.AddUniqueDynamic(this, &UUPGUWPauseMenu::OnSettingsButtonClicked);
    QuitGameButton->OnClicked.AddUniqueDynamic(this, &UUPGUWPauseMenu::OnQuitGameButtonClicked);
}

void UUPGUWPauseMenu::OnContinueButtonClicked()
{

}

void UUPGUWPauseMenu::OnSettingsButtonClicked()
{

}

void UUPGUWPauseMenu::OnQuitGameButtonClicked()
{
    UE_LOG(LogTemp, Log, TEXT("Quit!"))
    UKismetSystemLibrary::QuitGame(GetWorld(), GetOwningPlayer(), EQuitPreference::Quit, false);
}
