// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UPGUserWidget.generated.h"

UCLASS()
class UNIPOLY_API UUPGUserWidget : public UUserWidget
{
	GENERATED_BODY()
};
