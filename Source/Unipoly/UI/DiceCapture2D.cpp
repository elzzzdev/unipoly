#include "DiceCapture2D.h"

#include "Components/SceneCaptureComponent2D.h"

ADiceCapture2D::ADiceCapture2D()
{
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	
	DiceSkeletalMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("DiceSkeletalMesh"));
	DiceSkeletalMeshComponent->SetupAttachment(RootComponent);

	SceneCaptureComponent2D = CreateDefaultSubobject<USceneCaptureComponent2D>(TEXT("SceneCapture"));
	SceneCaptureComponent2D->SetupAttachment(RootComponent);
}

void ADiceCapture2D::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADiceCapture2D::Roll(const uint8 Result)
{
	if (DiceSkeletalMeshComponent)
	{
		USkeletalMesh* ResultMesh;
		switch (Result)
		{
		case 1:
			ResultMesh = DiceOneMesh;
			break;
		case 2:
			ResultMesh = DiceTwoMesh;
			break;
		case 3:
			ResultMesh = DiceThreeMesh;
			break;
		case 4:
			ResultMesh = DiceFourMesh;
			break;
		case 5:
			ResultMesh = DiceFiveMesh;
			break;
		default:
			ResultMesh = DiceSixMesh;
			break;
		}
		DiceSkeletalMeshComponent->SetSkeletalMesh(ResultMesh);

		const int32 AnimationIndex = FMath::RandRange(0, RollAnimations.Num() - 1);
		DiceSkeletalMeshComponent->SetAnimation(RollAnimations[AnimationIndex]);
		DiceSkeletalMeshComponent->Play(false);
	}
}

void ADiceCapture2D::Hide() const
{
	if (DiceSkeletalMeshComponent && DiceHideAnimation)
	{
		DiceSkeletalMeshComponent->SetAnimation(DiceHideAnimation);
		DiceSkeletalMeshComponent->Play(false);
	}
}

void ADiceCapture2D::BeginPlay()
{
	Super::BeginPlay();

	SceneCaptureComponent2D->ShowOnlyActorComponents(this);
}
