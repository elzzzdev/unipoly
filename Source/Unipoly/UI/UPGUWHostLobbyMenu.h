// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UPGUserWidget.h"
#include "UPGUWHostLobbyMenu.generated.h"

class UButton;

UCLASS()
class UNIPOLY_API UUPGUWHostLobbyMenu : public UUPGUserWidget
{
	GENERATED_BODY()
	
protected:
	virtual void NativeConstruct() override;
	
	UFUNCTION()
	void OnStartGameButtonClicked();
	
	UPROPERTY(meta = (BindWidget))
	UButton* StartGameButton;
};
