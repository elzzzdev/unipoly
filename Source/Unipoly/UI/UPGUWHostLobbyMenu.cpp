#include "UPGUWHostLobbyMenu.h"

#include "Kismet/GameplayStatics.h"
#include "Components/Button.h"

#include "Unipoly/GameplayFramework/UPGGameModeBase.h"

void UUPGUWHostLobbyMenu::NativeConstruct()
{
    Super::NativeConstruct();

    StartGameButton->OnClicked.AddUniqueDynamic(this, &UUPGUWHostLobbyMenu::OnStartGameButtonClicked);
}

void UUPGUWHostLobbyMenu::OnStartGameButtonClicked()
{
    AUPGGameModeBase* GameModeRef = Cast<AUPGGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
    if (GameModeRef->GetGameplayState() == EGameplayState::GameStarting)
    {
        GameModeRef->StartGame();
        UGameplayStatics::GetPlayerController(GetWorld(), 0)->SetShowMouseCursor(false);
        SetVisibility(ESlateVisibility::Collapsed);
    }
}
