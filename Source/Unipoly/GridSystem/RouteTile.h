// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RouteTile.generated.h"

class UUPGPawnSpotComponent;

UENUM(BlueprintType)
enum class ETileEventType : uint8
{
	None			UMETA(DisplayName = "None"),
	Class			UMETA(DisplayName = "Class"),
	Leisure			UMETA(DisplayName = "Leisure"),
	Gain			UMETA(DisplayName = "Gain"),
	Guile			UMETA(DisplayName = "Guile"),
	Weakening		UMETA(DisplayName = "Weakening"),
	Exam			UMETA(DisplayName = "Exam")
};

UCLASS()
class UNIPOLY_API ARouteTile : public AActor
{
	GENERATED_BODY()

public:
	ARouteTile();

	ARouteTile* GetNext() const;

	void SetOpened(bool Opened);

	bool IsOpened() const;

	void SetEventType(const ETileEventType NewType);

	ETileEventType GetEventType() const;

	UFUNCTION(NetMulticast, Reliable)
	void SetBottomMaterial(UMaterial* NewMaterial);

	UUPGPawnSpotComponent* GetNextAvailableSpot() const;

	UUPGPawnSpotComponent* GetSpotOccupiedBy(const APawn* OccupyingPawn) const;

	UFUNCTION(NetMulticast, Reliable)
	void PlayOpenAnimation();

protected:
	UPROPERTY(EditAnywhere)
	USkeletalMeshComponent* SkeletalMeshComponent;

	UPROPERTY(EditAnywhere)
	UUPGPawnSpotComponent* PawnSpotOne;

	UPROPERTY(EditAnywhere)
	UUPGPawnSpotComponent* PawnSpotTwo;

	UPROPERTY(EditAnywhere)
	UUPGPawnSpotComponent* PawnSpotThree;

	UPROPERTY(EditAnywhere)
	UUPGPawnSpotComponent* PawnSpotFour;

	UPROPERTY(EditAnywhere)
	TArray<UUPGPawnSpotComponent*> PawnSpots;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	UAnimSequence* OpenAnimation;

	UPROPERTY(EditInstanceOnly)
	ARouteTile* NextTile;

	UPROPERTY(Replicated, EditInstanceOnly)
	ETileEventType EventType;

	UPROPERTY(Replicated)
	bool bOpened = false;
};
