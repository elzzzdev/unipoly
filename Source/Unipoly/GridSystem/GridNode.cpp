// Fill out your copyright notice in the Description page of Project Settings.

#include "GridNode.h"

#include "RouteTile.h"
#include "Kismet/KismetMathLibrary.h"

AGridNode::AGridNode()
{
	PrimaryActorTick.bCanEverTick = true;
	
}

void AGridNode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGridNode::GenerateBasicTile(UStaticMesh* BasicTileMesh)
{
	if (!StaticMeshComponent)
	{
		StaticMeshComponent = NewObject<UStaticMeshComponent>(this);
		StaticMeshComponent->SetupAttachment(this->GetRootComponent());
		StaticMeshComponent->RegisterComponent();
	}
	StaticMeshComponent->SetStaticMesh(BasicTileMesh);
	
	UMaterialInstanceDynamic* DynamicMaterialInstance = UMaterialInstanceDynamic::Create(BasicTileMaterial, StaticMeshComponent);
	StaticMeshComponent->SetMaterial(0, DynamicMaterialInstance);
	
	DynamicMaterialInstance->SetVectorParameterValue(FName("Colour"), UKismetMathLibrary::HSVToRGB(FMath::RandRange(0.f, 1.f), 1, 1));
}

void AGridNode::BeginPlay()
{
	Super::BeginPlay();
	
}

void AGridNode::SwitchToRouteTile()
{
	if (bAsRouteTile)
	{
		return;
	}

	BasicTileMeshCache = StaticMeshComponent->GetStaticMesh();
	StaticMeshComponent->SetStaticMesh(nullptr);

	RouteTile = GetWorld()->SpawnActor<ARouteTile>(RouteTileBP);
	RouteTile->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);

	bAsRouteTile = true;
}

void AGridNode::SwitchToBasicTile()
{
	if (!bAsRouteTile)
	{
		return;
	}
	
	if (RouteTile)
	{
		RouteTile->Destroy();
		RouteTile = nullptr;
	}
	GenerateBasicTile(BasicTileMeshCache);
	bAsRouteTile = false;
}
