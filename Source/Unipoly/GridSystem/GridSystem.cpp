// Fill out your copyright notice in the Description page of Project Settings.

#include "GridSystem.h"

#include "GridNode.h"

AGridSystem::AGridSystem()
{
	PrimaryActorTick.bCanEverTick = true;

}

void AGridSystem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGridSystem::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
	
}

void AGridSystem::BeginPlay()
{
	Super::BeginPlay();
	
}

void AGridSystem::CleanGridStorage()
{
	for (AGridNode* Node : GridStorage)
	{
		Node->Destroy();
	}
	GridStorage.Empty();
}

void AGridSystem::RegenerateGrid()
{
	CleanGridStorage();

	// Spacing math source: redblobgames.com/grids/hexagons
	FVector2D TileSpacing(BasicTileMesh->GetBoundingBox().Max.X * 2, BasicTileMesh->GetBoundingBox().Max.Y * (3. / 2));
	
	for (uint32 i = 0; i < Height; ++i)
	{
		for (uint32 q = 0; q < Width; ++q)
		{
			AGridNode* NewNode = GetWorld()->SpawnActor<AGridNode>(GridNodeBP);
			NewNode->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
			NewNode->GenerateBasicTile(BasicTileMesh);

			FVector NewLocation = FVector(TileSpacing.X * q, TileSpacing.Y * i, 0);
			if (i % 2 == 0)
			{
				NewLocation.X +=  BasicTileMesh->GetBoundingBox().Max.X;
			}
			NewNode->SetActorRelativeLocation(NewLocation);
			GridStorage.Add(NewNode);
		}
	}
}
