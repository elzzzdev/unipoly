// Fill out your copyright notice in the Description page of Project Settings.

#include "RouteTile.h"

#include "Net/UnrealNetwork.h"
#include "Unipoly/Components/UPGPawnSpotComponent.h"

ARouteTile::ARouteTile()
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	SkeletalMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));
	SkeletalMeshComponent->SetupAttachment(RootComponent);

	PawnSpotOne = CreateDefaultSubobject<UUPGPawnSpotComponent>(TEXT("PawnSpotOne"));
	PawnSpotTwo = CreateDefaultSubobject<UUPGPawnSpotComponent>(TEXT("PawnSpotTwo"));
	PawnSpotThree = CreateDefaultSubobject<UUPGPawnSpotComponent>(TEXT("PawnSpotThree"));
	PawnSpotFour = CreateDefaultSubobject<UUPGPawnSpotComponent>(TEXT("PawnSpotFour"));

	PawnSpots.Add(PawnSpotOne);
	PawnSpots.Add(PawnSpotTwo);
	PawnSpots.Add(PawnSpotThree);
	PawnSpots.Add(PawnSpotFour);

	for (UUPGPawnSpotComponent* PawnSpot : PawnSpots)
	{
		PawnSpot->SetRelativeRotation(FRotator(90, 0, 0));
		// PawnSpot->ArrowSize = 40.f;
		PawnSpot->SetIsScreenSizeScaled(true);
		PawnSpot->SetupAttachment(RootComponent);
	}
}

ARouteTile* ARouteTile::GetNext() const
{
	return NextTile;
}

void ARouteTile::SetOpened(bool Opened)
{
	bOpened = Opened;
}

bool ARouteTile::IsOpened() const
{
	return bOpened;
}

void ARouteTile::SetEventType(const ETileEventType NewType)
{
	EventType = NewType;
}

ETileEventType ARouteTile::GetEventType() const
{
	return EventType;
}

void ARouteTile::SetBottomMaterial_Implementation(UMaterial* NewMaterial)
{
	SkeletalMeshComponent->SetMaterial(1, NewMaterial);
}

UUPGPawnSpotComponent* ARouteTile::GetNextAvailableSpot() const
{
	for (UUPGPawnSpotComponent* PawnSpot : PawnSpots)
	{
		if (!PawnSpot->isOccupied()) return PawnSpot;
	}
	return nullptr;
}

UUPGPawnSpotComponent* ARouteTile::GetSpotOccupiedBy(const APawn* OccupyingPawn) const
{
	for (UUPGPawnSpotComponent* PawnSpot : PawnSpots)
	{
		if (PawnSpot->OccupyingPawn == OccupyingPawn) return PawnSpot;
	}
	return nullptr;
}

void ARouteTile::PlayOpenAnimation_Implementation()
{
	SkeletalMeshComponent->PlayAnimation(OpenAnimation, false);
}

void ARouteTile::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	DOREPLIFETIME(ARouteTile, EventType)
	DOREPLIFETIME(ARouteTile, bOpened)
}
