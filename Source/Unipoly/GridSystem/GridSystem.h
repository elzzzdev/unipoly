// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GridSystem.generated.h"

class AGridNode;

UCLASS()
class UNIPOLY_API AGridSystem : public AActor
{
	GENERATED_BODY()
	
public:	
	AGridSystem();
	
	virtual void Tick(float DeltaTime) override;

	virtual void OnConstruction(const FTransform& Transform) override;

	UPROPERTY(EditAnywhere, Category="Grid System")
	uint32 Height = 3; 

	UPROPERTY(EditAnywhere, Category="Grid System")
	uint32 Width = 3;

protected:
	virtual void BeginPlay() override;

	UFUNCTION(CallInEditor, Category="Grid System")
	void CleanGridStorage();
	
	UFUNCTION(CallInEditor, Category="Grid System")
	void RegenerateGrid();

	UPROPERTY()
	TArray<AGridNode*> GridStorage;

	UPROPERTY(EditDefaultsOnly)
	UStaticMesh* BasicTileMesh;
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AGridNode> GridNodeBP;
};
