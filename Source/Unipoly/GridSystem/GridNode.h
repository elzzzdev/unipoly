// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GridNode.generated.h"

class ARouteTile;

UCLASS()
class UNIPOLY_API AGridNode : public AActor
{
	GENERATED_BODY()
	
public:	
	AGridNode();

	virtual void Tick(float DeltaTime) override;

	void GenerateBasicTile(UStaticMesh* BasicTileMesh);
	
protected:
	virtual void BeginPlay() override;

	UFUNCTION(CallInEditor)
	void SwitchToRouteTile();

	UFUNCTION(CallInEditor)
	void SwitchToBasicTile();

	bool bAsRouteTile;
	
	UPROPERTY()
	UStaticMeshComponent* StaticMeshComponent;

	UPROPERTY()
	ARouteTile* RouteTile = nullptr;

	UPROPERTY()
	UStaticMesh* BasicTileMeshCache = nullptr;

	UPROPERTY(EditDefaultsOnly)
	UMaterial* BasicTileMaterial;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ARouteTile> RouteTileBP;
};
