// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/TimelineComponent.h"
#include "UPGCamera.generated.h"

class USpringArmComponent;
class UCameraComponent;

UCLASS()
class UNIPOLY_API AUPGCamera : public AActor
{
	GENERATED_BODY()

public:
	AUPGCamera();

	virtual void Tick(float DeltaTime) override;

	void SetAlwaysRotateToAnchor(bool NewAlwaysRotateToAnchor);

	bool IsAlwaysRotateToAnchor() const;

	UFUNCTION(NetMulticast, Reliable)
	void NetMulticast_MoveToLocation(const FVector& Destination);

	UFUNCTION()
	void MovementTimelineProgress(float Alpha);

	UFUNCTION()
	void MovementTimelineFinished();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UCameraComponent* CameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USpringArmComponent* SpringArmComponent;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	UCurveFloat* MovementCurveFloat;

	UPROPERTY(EditInstanceOnly)
	AActor* RotationAnchor;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	bool bAlwaysRotateToAnchor = true;

	FVector StartLocation;

	FVector EndLocation;

	FTimeline MovementTimeline;
};
