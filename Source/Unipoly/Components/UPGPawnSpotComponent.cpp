// Fill out your copyright notice in the Description page of Project Settings.

#include "UPGPawnSpotComponent.h"

void UUPGPawnSpotComponent::SetOccupiedByPawn(APawn* NewOccupyingPawn)
{
    OccupyingPawn = NewOccupyingPawn;
}

void UUPGPawnSpotComponent::ClearOccupation()
{
    OccupyingPawn = nullptr;
}

bool UUPGPawnSpotComponent::isOccupied() const
{
    return OccupyingPawn ? true : false;
}
