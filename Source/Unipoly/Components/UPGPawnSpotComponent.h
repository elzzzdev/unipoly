// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ArrowComponent.h"
#include "UPGPawnSpotComponent.generated.h"

UCLASS()
class UNIPOLY_API UUPGPawnSpotComponent : public UArrowComponent
{
	GENERATED_BODY()

public:
	void SetOccupiedByPawn(APawn* NewOccupyingPawn);

	void ClearOccupation();

	bool isOccupied() const;

	UPROPERTY()
	APawn* OccupyingPawn;
};
