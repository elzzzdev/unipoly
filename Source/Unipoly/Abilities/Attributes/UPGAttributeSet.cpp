// Fill out your copyright notice in the Description page of Project Settings.

#include "UPGAttributeSet.h"

#include "Net/UnrealNetwork.h"

UUPGAttributeSet::UUPGAttributeSet()
{
	InitMana(0);
	InitKnowledge(0);
}

void UUPGAttributeSet::OnRep_Mana(const FGameplayAttributeData& OldMana)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UUPGAttributeSet, Mana, OldMana);
}

void UUPGAttributeSet::OnRep_Knowledge(const FGameplayAttributeData& OldKnowledge)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UUPGAttributeSet, Knowledge, OldKnowledge);
}

void UUPGAttributeSet::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION_NOTIFY(UUPGAttributeSet, Mana, COND_None, REPNOTIFY_Always)
	DOREPLIFETIME_CONDITION_NOTIFY(UUPGAttributeSet, Knowledge, COND_None, REPNOTIFY_Always)
}
