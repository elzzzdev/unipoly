// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "AbilitySystemComponent.h"
#include "UPGAttributeSet.generated.h"

// Uses macros from AttributeSet.h
#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)

UCLASS()
class UNIPOLY_API UUPGAttributeSet : public UAttributeSet
{
	GENERATED_BODY()

public:
	UUPGAttributeSet();

	UPROPERTY(BlueprintReadOnly, Category = "Mana", ReplicatedUsing = OnRep_Mana)
	FGameplayAttributeData Mana;
	ATTRIBUTE_ACCESSORS(UUPGAttributeSet, Mana)

	UPROPERTY(BlueprintReadOnly, Category = "Knowledge", ReplicatedUsing = OnRep_Knowledge)
	FGameplayAttributeData Knowledge;
	ATTRIBUTE_ACCESSORS(UUPGAttributeSet, Knowledge)

private:
	UFUNCTION()
	virtual void OnRep_Mana(const FGameplayAttributeData& OldMana);

	UFUNCTION()
	virtual void OnRep_Knowledge(const FGameplayAttributeData& OldKnowledge);
};
