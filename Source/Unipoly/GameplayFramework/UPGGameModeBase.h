// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UPGGameModeBase.generated.h"

class AUPGPlayerState;
class UUPGUWPlayersDeck;
class AUPGCamera;
class ARouteTile;
class ADiceCapture2D;
enum class ETileEventType : uint8;

UENUM(BlueprintType)
enum class EGameplayState : uint8
{
	GameStarting		UMETA(DisplayName = "GameStarting"),
    DiceRollPending		UMETA(DisplayName = "DiceRollPending"),
	RollingDice			UMETA(DisplayName = "RollingDice"),
	PawnMoving			UMETA(DisplayName = "PawnMoving")
};

UCLASS()
class UNIPOLY_API AUPGGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	AUPGGameModeBase();

	virtual void Tick(float DeltaTime) override;

	virtual void PostLogin(APlayerController* NewPlayer) override;

	virtual void RestartPlayer(AController* NewPlayer) override;

	UFUNCTION(BlueprintCallable)
    void SetGameplayState(EGameplayState NewState);

	UFUNCTION(BlueprintCallable)
    EGameplayState GetGameplayState() const;

	UFUNCTION()
    AUPGPlayerState* GetCurrentPlayer() const;

	UFUNCTION()
	AUPGCamera* GetCamera() const;

	const TDoubleLinkedList<ARouteTile*>* GetRouteTiles() const;

	UFUNCTION()
    void StartGame();

	UFUNCTION()
    bool MoveAttemptByPlayer(const AUPGPlayerState* PlayerState);

	UFUNCTION()
    void MoveFinishedByPlayer(const AUPGPlayerState* PlayerState);

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TArray<FColor> ColourPalette;

private:
	void RouteInit();

	void GenerateTile(ARouteTile* Tile) const;

	void DiceRollAnimationDone();

	EGameplayState GameplayState;

	UPROPERTY(EditDefaultsOnly)
	TMap<ETileEventType, UMaterial*> EventMaterialMap;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UUPGUWPlayersDeck> PlayersDeckClass;

	UPROPERTY()
	AUPGCamera* Camera;

	UPROPERTY()
    ARouteTile* HeadTile;

	UPROPERTY()
	ADiceCapture2D* DiceCapture2D;

    TDoubleLinkedList<ARouteTile*> RouteTiles;

	TQueue<AUPGPlayerState*> PlayersQueue;

	int8 LastDiceResult = 0;
};
