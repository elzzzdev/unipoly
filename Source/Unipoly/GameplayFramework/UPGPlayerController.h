// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UPGPlayerController.generated.h"

class UInputMappingContext;
class UInputAction;

UCLASS()
class UNIPOLY_API AUPGPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	AUPGPlayerController();

	virtual void Tick(float DeltaTime) override;

	virtual void PlayerTick(float DeltaTime) override;

	virtual void SetupInputComponent() override;

protected:
	virtual void BeginPlay() override;
	
	UFUNCTION(Server, Unreliable)
    void Server_RollDice(const FInputActionValue& Value);

	UPROPERTY(EditDefaultsOnly, Category = Input)
	UInputMappingContext* GameplayMappingContext;

	UPROPERTY(EditDefaultsOnly, Category = Input)
	UInputAction* DiceRollAction;
};
