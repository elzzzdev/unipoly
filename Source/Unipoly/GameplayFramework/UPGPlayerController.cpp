// Fill out your copyright notice in the Description page of Project Settings.

#include "UPGPlayerController.h"

#include "Kismet/GameplayStatics.h"
#include "Components/InputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "EnhancedInputComponent.h"

#include "UPGPlayerState.h"
#include "UPGGameModeBase.h"
#include "Unipoly/UPGCamera.h"

AUPGPlayerController::AUPGPlayerController()
{
    bAutoManageActiveCameraTarget = false;
}

void AUPGPlayerController::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

}

void AUPGPlayerController::PlayerTick(float DeltaTime)
{
    Super::PlayerTick(DeltaTime);

}

void AUPGPlayerController::SetupInputComponent()
{
    Super::SetupInputComponent();

    if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(InputComponent))
    {
        EnhancedInputComponent->BindAction(DiceRollAction, ETriggerEvent::Triggered, this, &AUPGPlayerController::Server_RollDice);
    }
}

void AUPGPlayerController::BeginPlay()
{
    Super::BeginPlay();

    // Set single camera for all players
    if(AUPGCamera* Camera = Cast<AUPGCamera>(UGameplayStatics::GetActorOfClass(GetWorld(), AUPGCamera::StaticClass())))
    {
        SetViewTarget(Camera);
    }

    if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()))
    {
        Subsystem->AddMappingContext(GameplayMappingContext, 0);
    }
}

void AUPGPlayerController::Server_RollDice_Implementation(const FInputActionValue& Value)
{
    AUPGGameModeBase* GN = Cast<AUPGGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
    GN->MoveAttemptByPlayer(GetPlayerState<AUPGPlayerState>());
}
