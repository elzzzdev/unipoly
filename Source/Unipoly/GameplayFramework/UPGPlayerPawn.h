// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "AbilitySystemInterface.h"
#include "Components/TimelineComponent.h"
#include "UPGPlayerPawn.generated.h"

class AUPGPlayerController;
class AUPGGameModeBase;
class ARouteTile;
class UCurveFloat;

UCLASS()
class UNIPOLY_API AUPGPlayerPawn : public APawn, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	AUPGPlayerPawn();

	virtual void Tick(float DeltaTime) override;

	virtual void PossessedBy(AController* NewController) override;

	virtual void OnRep_PlayerState() override;

	virtual void OnRep_Controller() override;

	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	void SetCurrentTileNode(TDoubleLinkedList<ARouteTile*>::TDoubleLinkedListNode* NewCurrentTileNode);

	TDoubleLinkedList<ARouteTile*>::TDoubleLinkedListNode* GetCurrentTileNode() const;

	UFUNCTION(NetMulticast, Reliable)
	void NetMulticast_PlayRiseAnimation() const;

	UFUNCTION(Server, Reliable)
	void Server_TakeStepsForward(uint16 Steps);

	UFUNCTION()
	bool IsWalking() const;

protected:
	virtual void BeginPlay() override;

	UFUNCTION(Server, Reliable)
    void Server_TakeNextStep();

	UFUNCTION(NetMulticast, Reliable)
	void NetMulticast_StartMovementTo(const FVector NewLocation);

	UFUNCTION(NetMulticast, Reliable)
    void NetMulticast_StartRotationTo(const FRotator NewRotation);

	UFUNCTION()
	void MovementTimelineProgress(float Alpha);

	UFUNCTION()
    void MovementTimelineFinished();

	UFUNCTION()
    void RotationTimelineProgress(float Alpha);

	UFUNCTION()
    void RotationTimelineFinished();

	UFUNCTION(Server, Reliable)
    void Server_MovementFinished();

	AUPGGameModeBase* GetGameMode() const;

	AUPGPlayerController* GetPlayerController() const;

	UPROPERTY(EditAnywhere)
	USkeletalMeshComponent* SkeletalMeshComponent;

	TWeakObjectPtr<UAbilitySystemComponent> AbilitySystemComponent;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	UAnimSequence* JumpOnMovementAnimation;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	UAnimSequence* RiseAnimation;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	UCurveFloat* MovementCurveFloat;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	UCurveFloat* RotationCurveFloat;

	UPROPERTY(Replicated)
	bool bWalking = false;

	int StepsToTakeCount;

	FVector StartLocation;

	FVector EndLocation;

	FRotator StartRotation;

	FRotator EndRotation;

	FTimeline MovementTimeline;

	FTimeline RotationTimeline;

	TDoubleLinkedList<ARouteTile*>::TDoubleLinkedListNode* CurrentTileNode;
};
