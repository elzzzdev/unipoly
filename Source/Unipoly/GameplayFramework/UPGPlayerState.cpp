// Fill out your copyright notice in the Description page of Project Settings.

#include "UPGPlayerState.h"

#include "AbilitySystemComponent.h"
#include "UPGGameStateBase.h"
#include "Net/UnrealNetwork.h"
#include "Unipoly/Abilities/Attributes/UPGAttributeSet.h"

AUPGPlayerState::AUPGPlayerState()
{
	AbilitySystemComponent = CreateDefaultSubobject<UAbilitySystemComponent>(TEXT("AbilitySystemComponent"));
	AbilitySystemComponent->SetIsReplicated(true);
	AbilitySystemComponent->SetReplicationMode(EGameplayEffectReplicationMode::Mixed);

	AttributeSet = CreateDefaultSubobject<UUPGAttributeSet>(TEXT("AttributeSet"));
}

UAbilitySystemComponent* AUPGPlayerState::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

UUPGAttributeSet* AUPGPlayerState::GetAttributeSet() const
{
	return AttributeSet;
}

float AUPGPlayerState::GetMana() const
{
	return AttributeSet->GetMana();
}

float AUPGPlayerState::GetKnowledge() const
{
	return AttributeSet->GetKnowledge();
}

void AUPGPlayerState::SetPlayerAvatar(UTexture2D* NewAvatar)
{
	PlayerAvatar = NewAvatar;
}

UTexture2D* AUPGPlayerState::GetPlayerAvatar() const
{
	return PlayerAvatar;
}

void AUPGPlayerState::SetPlayerColour(const FColor NewColour)
{
	PlayerColour = NewColour;
}

FColor AUPGPlayerState::GetPlayerColour() const
{
	return PlayerColour;
}

void AUPGPlayerState::BeginPlay()
{
	Super::BeginPlay();

	if (AbilitySystemComponent)
	{
		ManaChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetManaAttribute()).AddUObject(this, &AUPGPlayerState::ManaChanged);
		KnowledgeChangedDelegateHandle = AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSet->GetKnowledgeAttribute()).AddUObject(this, &AUPGPlayerState::KnowledgeChanged);
	}
}

void AUPGPlayerState::ManaChanged(const FOnAttributeChangeData& Data)
{
	const float Mana = Data.NewValue;
	if (const UWorld* World = GetWorld())
	{
		World->GetGameState<AUPGGameStateBase>()->Client_AddManaToPlayer(this, Mana);
	}
}

void AUPGPlayerState::KnowledgeChanged(const FOnAttributeChangeData& Data)
{
	const float Knowledge = Data.NewValue;
	if (const UWorld* World = GetWorld())
	{
		World->GetGameState<AUPGGameStateBase>()->Client_AddKnowledgeToPlayer(this, Knowledge);
	}
}

void AUPGPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	DOREPLIFETIME(AUPGPlayerState, PlayerColour)
}
