// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "UPGGameStateBase.generated.h"

class UUPGUWPendingHint;
class AUPGPlayerState;
class UUPGUWPlayersDeck;

UCLASS()
class UNIPOLY_API AUPGGameStateBase : public AGameStateBase
{
	GENERATED_BODY()

public:
	AUPGGameStateBase();

	UFUNCTION(NetMulticast, Reliable)
	void NetMulticast_CreatePendingHintWidget();

	UFUNCTION(NetMulticast, Reliable)
	void NetMulticast_ShowPendingHintForPlayer(const AUPGPlayerState* Player);

	UFUNCTION(NetMulticast, Reliable)
	void NetMulticast_HidePendingHint();

	UFUNCTION(NetMulticast, Reliable)
	void NetMulticast_CreatePlayerDeckWidget(const TArray<AUPGPlayerState*>& Players);

	UFUNCTION(Client, Reliable)
	void Client_AddManaToPlayer(const AUPGPlayerState* Player, const uint16 Amount);

	UFUNCTION(Client, Reliable)
	void Client_SubtractManaToPlayer(const AUPGPlayerState* Player, const uint16 Amount);

	UFUNCTION(Client, Reliable)
	void Client_AddKnowledgeToPlayer(const AUPGPlayerState* Player, const uint16 Amount);

	UFUNCTION(Client, Reliable)
	void Client_SubtractKnowledgeToPlayer(const AUPGPlayerState* Player, const uint16 Amount);

	UFUNCTION(NetMulticast, Reliable)
	void NetMulticast_PickPlayer(const AUPGPlayerState* Player);

private:
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UUPGUWPendingHint> PendingHintClass;

	UPROPERTY()
	UUPGUWPendingHint* PendingHint;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UUPGUWPlayersDeck> PlayersDeckClass;

	UPROPERTY()
	UUPGUWPlayersDeck* PlayersDeck;
};
