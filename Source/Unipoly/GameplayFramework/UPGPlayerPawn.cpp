// Fill out your copyright notice in the Description page of Project Settings.

#include "UPGPlayerPawn.h"

#include "AbilitySystemComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

#include "UPGPlayerController.h"
#include "UPGGameModeBase.h"
#include "UPGPlayerState.h"
#include "Unipoly/UPGCamera.h"
#include "Unipoly/Components/UPGPawnSpotComponent.h"
#include "Unipoly/GridSystem/RouteTile.h"

AUPGPlayerPawn::AUPGPlayerPawn()
{
	PrimaryActorTick.bCanEverTick = true;
    bReplicates = true;
	// AutoPossessPlayer = EAutoReceiveInput::Player0;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

    SkeletalMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));
    SkeletalMeshComponent->SetupAttachment(RootComponent);
}

void AUPGPlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

    MovementTimeline.TickTimeline(DeltaTime);
    RotationTimeline.TickTimeline(DeltaTime);
}

void AUPGPlayerPawn::PossessedBy(AController* NewController)
{
    Super::PossessedBy(NewController);

    if (AbilitySystemComponent == nullptr)
    {
        if (AUPGPlayerState* PS = GetPlayerState<AUPGPlayerState>())
        {
            AbilitySystemComponent = PS->GetAbilitySystemComponent();
            AbilitySystemComponent->InitAbilityActorInfo(PS, this);

            // Some games grant attributes and abilities here

            // Some games server initialize another components of the character that use the ASC here
        }
    }
}

void AUPGPlayerPawn::OnRep_PlayerState()
{
    Super::OnRep_PlayerState();
    if (AbilitySystemComponent == nullptr)
    {
        if (AUPGPlayerState* PS = GetPlayerState<AUPGPlayerState>())
        {
            AbilitySystemComponent = PS->GetAbilitySystemComponent();
            AbilitySystemComponent->InitAbilityActorInfo(PS, this);

            // Some games grant attributes here

            // Some games client initialize another components of the character that use the ASC here
        }
    }
    else
    {
        // Solves the data-races of controller/playerstate
        AbilitySystemComponent->RefreshAbilityActorInfo();
    }
}

void AUPGPlayerPawn::OnRep_Controller()
{
    Super::OnRep_Controller();
    // Needed in case the PC wasn't valid when we Init-ed the ASC.
    if (AUPGPlayerState* PS = GetPlayerState<AUPGPlayerState>())
    {
        PS->GetAbilitySystemComponent()->RefreshAbilityActorInfo();
    }
}

UAbilitySystemComponent* AUPGPlayerPawn::GetAbilitySystemComponent() const
{
    return AbilitySystemComponent.Get();
}

void AUPGPlayerPawn::SetCurrentTileNode(TDoubleLinkedList<ARouteTile*>::TDoubleLinkedListNode* NewCurrentRouteNode)
{
    CurrentTileNode = NewCurrentRouteNode;
}

TDoubleLinkedList<ARouteTile*>::TDoubleLinkedListNode* AUPGPlayerPawn::GetCurrentTileNode() const
{
    return CurrentTileNode;
}

void AUPGPlayerPawn::Server_TakeStepsForward_Implementation(uint16 Steps)
{
    StepsToTakeCount = Steps;
    Server_TakeNextStep();
}


bool AUPGPlayerPawn::IsWalking() const
{
    return bWalking;
}

void AUPGPlayerPawn::BeginPlay()
{
	Super::BeginPlay();

    if (MovementCurveFloat)
    {
        FOnTimelineFloat onMovementTimelineProgress;
        FOnTimelineEventStatic onMovementTimelineFinished;

        onMovementTimelineProgress.BindUFunction(this, FName("MovementTimelineProgress"));
        MovementTimeline.AddInterpFloat(MovementCurveFloat, onMovementTimelineProgress);

        onMovementTimelineFinished.BindUFunction(this, FName("MovementTimelineFinished"));
        MovementTimeline.SetTimelineFinishedFunc(onMovementTimelineFinished);
    }

    if (RotationCurveFloat)
    {
        FOnTimelineFloat onRotationTimelineProgress;
        FOnTimelineEventStatic onRotationTimelineFinished;

        onRotationTimelineProgress.BindUFunction(this, FName("RotationTimelineProgress"));
        RotationTimeline.AddInterpFloat(RotationCurveFloat, onRotationTimelineProgress);

        onRotationTimelineFinished.BindUFunction(this, FName("RotationTimelineFinished"));
        RotationTimeline.SetTimelineFinishedFunc(onRotationTimelineFinished);
    }
}

void AUPGPlayerPawn::NetMulticast_PlayRiseAnimation_Implementation() const
{
    SkeletalMeshComponent->PlayAnimation(RiseAnimation, false);
}

void AUPGPlayerPawn::Server_TakeNextStep_Implementation()
{
    if (!CurrentTileNode) return;
    StepsToTakeCount--;
    bWalking = true;

    const ARouteTile* CurrentTile = CurrentTileNode->GetValue();
    if (UUPGPawnSpotComponent* CurrentSpot = CurrentTile->GetSpotOccupiedBy(this))
    {
        CurrentSpot->ClearOccupation();
    }

    SetCurrentTileNode(CurrentTileNode->GetNextNode());

    if (!CurrentTileNode)
    {
        if (AUPGGameModeBase* GM = GetGameMode())
        {
            SetCurrentTileNode(GM->GetRouteTiles()->GetHead());
        }
    }

    if (CurrentTile && CurrentTileNode)
    {
        CurrentTile = CurrentTileNode->GetValue();
        if (UUPGPawnSpotComponent* NextSpot = CurrentTile->GetNextAvailableSpot())
        {
            NextSpot->SetOccupiedByPawn(this);
            NetMulticast_StartMovementTo(NextSpot->GetComponentLocation());

            if (CurrentTile->GetActorRotation() != GetActorRotation())
            {
                NetMulticast_StartRotationTo(CurrentTile->GetActorRotation());
            }

            if (AUPGGameModeBase* GM = GetGameMode())
            {
                GM->GetCamera()->NetMulticast_MoveToLocation(CurrentTile->GetActorLocation());
            }
        }
    }
}

void AUPGPlayerPawn::NetMulticast_StartMovementTo_Implementation(const FVector NewLocation)
{
    EndLocation = NewLocation;
    StartLocation = GetActorLocation();
    MovementTimeline.PlayFromStart();
    SkeletalMeshComponent->PlayAnimation(JumpOnMovementAnimation, false);
}

void AUPGPlayerPawn::NetMulticast_StartRotationTo_Implementation(const FRotator NewRotation)
{
    EndRotation = NewRotation;
    StartRotation = GetActorRotation();
    RotationTimeline.PlayFromStart();
}

void AUPGPlayerPawn::MovementTimelineProgress(float Alpha)
{
    SetActorLocation(FMath::Lerp(StartLocation, EndLocation, Alpha));
    // UE_LOG(LogTemp, Log, TEXT("MovementTimelineProgress: %f"), Alpha)
}

void AUPGPlayerPawn::MovementTimelineFinished()
{
    if (!StepsToTakeCount)
    {
        if (HasAuthority())
        {
            Server_MovementFinished();
        }
    }
    else
    {
        Server_TakeNextStep();
    }
}

void AUPGPlayerPawn::RotationTimelineProgress(float Alpha)
{
    SetActorRotation(FMath::Lerp(StartRotation, EndRotation, Alpha));
    // UE_LOG(LogTemp, Log, TEXT("RotationTimelineProgress: %f"), Alpha)
}

void AUPGPlayerPawn::RotationTimelineFinished()
{
    // UE_LOG(LogTemp, Log, TEXT("RotationTimelineProgress End"))
}


void AUPGPlayerPawn::Server_MovementFinished_Implementation()
{
    bWalking = false;
    if (AUPGGameModeBase* GM = GetGameMode())
    {
        GM->MoveFinishedByPlayer(Cast<AUPGPlayerState>(GetPlayerState()));
    }
}
AUPGGameModeBase* AUPGPlayerPawn::GetGameMode() const
{
    return Cast<AUPGGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
}

AUPGPlayerController* AUPGPlayerPawn::GetPlayerController() const
{
    return Cast<AUPGPlayerController>(GetController());
}

void AUPGPlayerPawn::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(AUPGPlayerPawn, bWalking)
}
