// Copyright Epic Games, Inc. All Rights Reserved.

#include "UPGGameModeBase.h"

#include "UPGGameStateBase.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/GameStateBase.h"
#include "GameFramework/PlayerState.h"

#include "Unipoly/UI/DiceCapture2D.h"
#include "Unipoly/GridSystem/RouteTile.h"
#include "Unipoly/Components/UPGPawnSpotComponent.h"
#include "UPGPlayerPawn.h"
#include "UPGPlayerState.h"
#include "Unipoly/UPGCamera.h"
#include "Unipoly/Abilities/Attributes/UPGAttributeSet.h"

AUPGGameModeBase::AUPGGameModeBase()
{
    PrimaryActorTick.bStartWithTickEnabled = true;
    PrimaryActorTick.bCanEverTick = true;

    SetGameplayState(EGameplayState::GameStarting);

    DiceCapture2D = Cast<ADiceCapture2D>(UGameplayStatics::GetActorOfClass(GetWorld(), ADiceCapture2D::StaticClass()));
    Camera = Cast<AUPGCamera>(UGameplayStatics::GetActorOfClass(GetWorld(), AUPGCamera::StaticClass()));

    TArray<AActor*> FoundTiles;
    UGameplayStatics::GetAllActorsOfClassWithTag(GetWorld(), ARouteTile::StaticClass(), TEXT("HeadTile"), FoundTiles);
    if (FoundTiles.Num())
    {
        HeadTile = Cast<ARouteTile>(FoundTiles[0]);
        RouteTiles.AddHead(HeadTile);
    }
}

void AUPGGameModeBase::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    if (GEngine)
    {
        GEngine->AddOnScreenDebugMessage(3, 1, FColor::White, FString::Printf(TEXT("Route length: %d"), RouteTiles.Num()));

        for (auto State : UGameplayStatics::GetGameState(GetWorld())->PlayerArray)
        {
            AUPGPlayerState* UPGState = Cast<AUPGPlayerState>(State);
            FString PlayerString = FString::Printf(TEXT("Player %d: %s | Ping: %f"), UPGState->GetPlayerId(), *UPGState->GetPlayerName(), UPGState->GetPingInMilliseconds());
            if (GameplayState != EGameplayState::GameStarting && UPGState == GetCurrentPlayer())
            {
                PlayerString += TEXT(" | It's their turn!");
            }
            GEngine->AddOnScreenDebugMessage(static_cast<int>(UPGState->GetUniqueID()), 1, UPGState->GetPlayerColour(), *PlayerString);
        }
    }
}

void AUPGGameModeBase::PostLogin(APlayerController* NewPlayer)
{
    Super::PostLogin(NewPlayer);

    // TODO Player check-in

    if (ColourPalette.Num())
    {
        const int32 Index = FMath::RandRange(0, ColourPalette.Num() - 1);
        NewPlayer->GetPlayerState<AUPGPlayerState>()->SetPlayerColour(ColourPalette[Index]);
        ColourPalette.RemoveAt(Index);
    }
}

void AUPGGameModeBase::RestartPlayer(AController* NewPlayer)
{
    Super::RestartPlayer(NewPlayer);
    // TODO? Rewrite with just a FindPlayerStart instead

    // Move pawn to it's spot on start tile
    AUPGPlayerPawn* NewPlayerPawn = Cast<AUPGPlayerPawn>(NewPlayer->GetPawn());
    NewPlayerPawn->SetCurrentTileNode(RouteTiles.GetHead());
    const ARouteTile* StartTile = RouteTiles.GetHead()->GetValue();
    if (UUPGPawnSpotComponent* PawnSpot = StartTile->GetNextAvailableSpot())
    {
        const FRotator StartRotation = StartTile->GetActorRotation();
        const FVector StartLocation = PawnSpot->GetComponentLocation();
        NewPlayerPawn->SetActorTransform(FTransform(StartRotation, StartLocation));
        PawnSpot->SetOccupiedByPawn(NewPlayerPawn);
    }
}

void AUPGGameModeBase::SetGameplayState(EGameplayState NewState)
{
    GameplayState = NewState;
}

EGameplayState AUPGGameModeBase::GetGameplayState() const
{
    return GameplayState;
}

AUPGPlayerState* AUPGGameModeBase::GetCurrentPlayer() const
{
    AUPGPlayerState* CurrentPlayer;
    PlayersQueue.Peek(CurrentPlayer);
    return CurrentPlayer;
}

AUPGCamera* AUPGGameModeBase::GetCamera() const
{
    return Camera;
}

const TDoubleLinkedList<ARouteTile*>* AUPGGameModeBase::GetRouteTiles() const
{
    return &RouteTiles;
}

void AUPGGameModeBase::StartGame()
{
    // Copy array of pointers to shuffle players order without affect on original PlayerArray
    TArray<AUPGPlayerState*> PlayerStates;
    PlayerStates.Append(GetGameState<AUPGGameStateBase>()->PlayerArray);
    const int32 LastIndex = PlayerStates.Num() - 1;
    for (int32 i = 0; i <= LastIndex; i += 1) {
        const int32 Index = FMath::RandRange(i, LastIndex);
        if (i == Index) {
            continue;
        }
        PlayerStates.Swap(i, Index);
    }
    for (auto State : PlayerStates)
    {
        PlayersQueue.Enqueue(State);
    }

    RouteInit();
    if (AUPGGameStateBase* GS = GetGameState<AUPGGameStateBase>())
    {
        GS->NetMulticast_CreatePlayerDeckWidget(PlayerStates);
        GS->NetMulticast_CreatePendingHintWidget();
        GS->NetMulticast_ShowPendingHintForPlayer(GetCurrentPlayer());
    }
    SetGameplayState(EGameplayState::DiceRollPending);
}

bool AUPGGameModeBase::MoveAttemptByPlayer(const AUPGPlayerState* PlayerState)
{
    if (GetGameplayState() != EGameplayState::DiceRollPending || GetCurrentPlayer() != PlayerState)
    {
        return false;
    }
    SetGameplayState(EGameplayState::RollingDice);
    GetGameState<AUPGGameStateBase>()->NetMulticast_HidePendingHint();
    GetGameState<AUPGGameStateBase>()->NetMulticast_PickPlayer(GetCurrentPlayer());

    if (const AUPGPlayerPawn* PlayerPawn = Cast<AUPGPlayerPawn>(PlayerState->GetPawn()))
    {
        Camera->NetMulticast_MoveToLocation(PlayerPawn->GetCurrentTileNode()->GetValue()->GetActorLocation());
    }

    const int32 DiceResult = FMath::RandRange(1, 6);
    LastDiceResult = DiceResult;
    DiceCapture2D->Roll(DiceResult);
    FTimerHandle DiceRollAnimationHandle;
    GetWorldTimerManager().SetTimer(DiceRollAnimationHandle, this, &AUPGGameModeBase::DiceRollAnimationDone, 4.75f, false);
    return true;
}

void AUPGGameModeBase::MoveFinishedByPlayer(const AUPGPlayerState* PlayerState)
{
    if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 3, FColor::Silver, FString(TEXT("Move Finished!")));
    const AUPGPlayerPawn* PlayerPawn = Cast<AUPGPlayerPawn>(PlayerState->GetPawn());

    TDoubleLinkedList<ARouteTile*>::TDoubleLinkedListNode* CurrentTileNode = PlayerPawn->GetCurrentTileNode();
    if (ARouteTile* CurrentTile = CurrentTileNode->GetValue())
    {
        if (!CurrentTile->IsOpened())
        {
            GenerateTile(CurrentTile);
            CurrentTile->SetOpened(true);
            CurrentTile->PlayOpenAnimation();
            PlayerPawn->NetMulticast_PlayRiseAnimation();
        }

        switch (CurrentTile->GetEventType())
        {
            case ETileEventType::Class:
                PlayerState->GetAttributeSet()->SetKnowledge(PlayerState->GetAttributeSet()->GetKnowledge() + 1);
                break;
            case ETileEventType::Leisure:
                PlayerState->GetAttributeSet()->SetMana(PlayerState->GetAttributeSet()->GetMana() + 1);
                break;
            case ETileEventType::Guile:
                break;
            case ETileEventType::Gain:
                break;
            case ETileEventType::Weakening:
                break;
            case ETileEventType::Exam:
                break;
            default:
                break;
        }
    }

    AUPGPlayerState* CurrentPlayer;
    PlayersQueue.Dequeue(CurrentPlayer);
    PlayersQueue.Enqueue(CurrentPlayer);

    SetGameplayState(EGameplayState::DiceRollPending);
    GetGameState<AUPGGameStateBase>()->NetMulticast_ShowPendingHintForPlayer(GetCurrentPlayer());
    DiceCapture2D->Hide();
}

void AUPGGameModeBase::RouteInit()
{
    uint16 PredictedRouteSize;
    {
        TArray<AActor*> FoundTiles;
        UGameplayStatics::GetAllActorsOfClass(GetWorld(), ARouteTile::StaticClass(), FoundTiles);
        PredictedRouteSize = FoundTiles.Num();
    }

    // Randomly choose exam tile on second half of route
    const uint16 ExamTileNum = FMath::RandRange(PredictedRouteSize / 2, PredictedRouteSize);

    // Load all route tiles in order, starting from head tile
    uint16 CurrentTileNum = 1;
    ARouteTile* CurrentTile = HeadTile;
    while (true)
    {
        CurrentTileNum++;
        CurrentTile = CurrentTile->GetNext();
        if (!CurrentTile) break;

        /* This code will impact HeadTile */

        if (CurrentTile == HeadTile) break;

        if (CurrentTileNum == ExamTileNum)
        {
            CurrentTile->SetEventType(ETileEventType::Exam);
            UMaterial* NewMaterial = EventMaterialMap.FindRef(ETileEventType::Exam);
            CurrentTile->SetBottomMaterial(NewMaterial);
            if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Silver, FString::Printf(TEXT("Exam on %d tile"), ExamTileNum));
        }

        RouteTiles.AddTail(CurrentTile);
    }

}

void AUPGGameModeBase::GenerateTile(ARouteTile* Tile) const
{
    /*  Generate tile type by chance.
    *  Class - 25%; Leisure - 25%; Guile - 20%; Gain - 15%; Weakening - 15%;
    *  Exam tile is generated on route init.
    */
    if (Tile->GetEventType() != ETileEventType::Exam)
    {
        const float TileTypeChance = FMath::RandRange(0.f, 1.f);
        if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Silver, FString::Printf(TEXT("Tile type chance: %f"), TileTypeChance));

        ETileEventType NewTileType = ETileEventType::Class;
        if (TileTypeChance < .25f)
        {
            NewTileType = ETileEventType::Class;
        }
        else if (TileTypeChance < .50f)
        {
            NewTileType = ETileEventType::Leisure;
        }
        else if (TileTypeChance < .70f)
        {
            NewTileType = ETileEventType::Guile;
        }
        else if (TileTypeChance < .85f)
        {
            NewTileType = ETileEventType::Gain;
        }
        else if (TileTypeChance <= 1.f)
        {
            NewTileType = ETileEventType::Weakening;
        }
        Tile->SetEventType(NewTileType);

        UMaterial* NewMaterial = EventMaterialMap.FindRef(NewTileType);
        Tile->SetBottomMaterial(NewMaterial);
    }
}

void AUPGGameModeBase::DiceRollAnimationDone()
{
    SetGameplayState(EGameplayState::PawnMoving);
    Cast<AUPGPlayerPawn>(GetCurrentPlayer()->GetPawn())->Server_TakeStepsForward(LastDiceResult);
}
