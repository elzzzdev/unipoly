// Copyright Epic Games, Inc. All Rights Reserved.

#include "UPGGameStateBase.h"

#include "UPGPlayerState.h"
#include "Blueprint/UserWidget.h"
#include "Components/CanvasPanelSlot.h"
#include "GameFramework/PlayerState.h"
#include "Unipoly/UI/UPGUWPendingHint.h"
#include "Unipoly/UI/UPGUWPlayerPlate.h"
#include "Unipoly/UI/UPGUWPlayersDeck.h"

AUPGGameStateBase::AUPGGameStateBase()
{
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;
}

void AUPGGameStateBase::NetMulticast_CreatePendingHintWidget_Implementation()
{
	PendingHint = CreateWidget<UUPGUWPendingHint>(GetWorld(), PendingHintClass);
	PendingHint->AddToViewport();
	PendingHint->SetAlignmentInViewport(FVector2D(0, 1));
	PendingHint->SetPositionInViewport(FVector2D(0, -10));
	PendingHint->SetAnchorsInViewport(FAnchors(0, 1, 1, 1));
}

void AUPGGameStateBase::NetMulticast_ShowPendingHintForPlayer_Implementation(const AUPGPlayerState* Player)
{
	const AUPGPlayerState* LocalPlayer = GetWorld()->GetFirstPlayerController()->GetPlayerState<AUPGPlayerState>();
	if (LocalPlayer->GetUniqueID() == Player->GetUniqueID())
	{
		PendingHint->ShowPressHint();
	}
	else
	{
		PendingHint->ShowWaitForPlayer(FText::FromString(Player->GetPlayerName()), Player->GetPlayerColour());
	}
}

void AUPGGameStateBase::NetMulticast_HidePendingHint_Implementation()
{
	PendingHint->Hide();
}

void AUPGGameStateBase::NetMulticast_CreatePlayerDeckWidget_Implementation(const TArray<AUPGPlayerState*>& Players)
{
	PlayersDeck = CreateWidget<UUPGUWPlayersDeck>(GetWorld(), PlayersDeckClass);
	PlayersDeck->AddToViewport();
	PlayersDeck->SetAlignmentInViewport(FVector2D(0, 1));
	PlayersDeck->SetAnchorsInViewport(FAnchors(0, 1));

	for(const AUPGPlayerState* Player : Players)
	{
		PlayersDeck->AddPlayerPlate(Player);
	}
}

void AUPGGameStateBase::Client_AddManaToPlayer_Implementation(const AUPGPlayerState* Player, const uint16 Amount)
{
	PlayersDeck->GetPlayerPlate(Player)->AddManaAmount(Amount);
}

void AUPGGameStateBase::Client_SubtractManaToPlayer_Implementation(const AUPGPlayerState* Player, const uint16 Amount)
{
	PlayersDeck->GetPlayerPlate(Player)->SubtractManaAmount(Amount);
}

void AUPGGameStateBase::Client_AddKnowledgeToPlayer_Implementation(const AUPGPlayerState* Player, const uint16 Amount)
{
	PlayersDeck->GetPlayerPlate(Player)->AddKnowledgeAmount(Amount);
}

void AUPGGameStateBase::Client_SubtractKnowledgeToPlayer_Implementation(const AUPGPlayerState* Player, const uint16 Amount)
{
	PlayersDeck->GetPlayerPlate(Player)->SubtractKnowledgeAmount(Amount);
}

void AUPGGameStateBase::NetMulticast_PickPlayer_Implementation(const AUPGPlayerState* Player)
{
	PlayersDeck->DiscardCurrentPick();
	PlayersDeck->PickPlayer(Player);
}
