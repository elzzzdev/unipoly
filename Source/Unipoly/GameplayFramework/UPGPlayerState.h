// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "AbilitySystemInterface.h"
#include "GameplayEffectTypes.h"
#include "UPGPlayerState.generated.h"

class UUPGAttributeSet;

UCLASS()
class UNIPOLY_API AUPGPlayerState : public APlayerState, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	AUPGPlayerState();

	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	class UUPGAttributeSet* GetAttributeSet() const;

	UFUNCTION(BlueprintGetter)
	float GetMana() const;

	UFUNCTION(BlueprintGetter)
	float GetKnowledge() const;

	UFUNCTION(BlueprintSetter)
	void SetPlayerAvatar(UTexture2D* NewAvatar);

	UFUNCTION(BlueprintGetter)
	UTexture2D* GetPlayerAvatar() const;

	UFUNCTION(BlueprintSetter)
	void SetPlayerColour(const FColor NewColour);

	UFUNCTION(BlueprintGetter)
	FColor GetPlayerColour() const;

private:
	virtual void BeginPlay() override;

	virtual void ManaChanged(const FOnAttributeChangeData& Data);

	virtual void KnowledgeChanged(const FOnAttributeChangeData& Data);

	UPROPERTY()
	UAbilitySystemComponent* AbilitySystemComponent;

	UPROPERTY()
	UUPGAttributeSet* AttributeSet;

	UPROPERTY(Replicated, EditAnywhere, BlueprintGetter=GetPlayerAvatar, BlueprintSetter=SetPlayerAvatar)
	UTexture2D* PlayerAvatar;

	UPROPERTY(Replicated, EditInstanceOnly, BlueprintGetter=GetPlayerColour, BlueprintSetter=SetPlayerColour)
	FColor PlayerColour = FColor::White;

	FDelegateHandle ManaChangedDelegateHandle;
	FDelegateHandle KnowledgeChangedDelegateHandle;
};
